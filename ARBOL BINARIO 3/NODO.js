class Nodo{
    constructor(nodoPadre = null, posicion = null,nombre) {
        this.nombre = nombre;
        this.padre = nodoPadre;
        this.posicion = posicion;
        if(nodoPadre == null)
            this.nivel = 0;
        else
            this.nivel = nodoPadre.nivel + 1;
    }
}