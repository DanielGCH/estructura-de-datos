class Arbol{
    constructor() {
        this.nodoPadre = this.NodoPadre();
        //this.nivel - 2;
        //this.buscarNivel=[];
        //this.busquedaElemento;
        //this.caminoNodo = " ";
        //this.sumaDeCaminos =0;
    }

    NodoPadre(padre,nivel,posicion,valor,nombre){
        var nodo = new Nodo(padre,nivel,posicion,valor,nombre);
        return nodo;
    }

    Nodo(padre,nivel,posicion,valor,nombre){
        var nodo = new Nodo(padre,nivel,posicion,valor,nombre);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if (nodo.nivel == this.nivel)
            this.buscarNivel.push(nodo.valor);

        if (nodo.has0wnProperty(v:"hIzq"))
            this.verificarNivelHijos(nodo.hIzq);

        if (nodo.has0wnProperty(v:"hDer"))
            this.verificarNivelHijos(nodo.hDer);

        return this.buscarNivel;

    }
    buscarNombre(buscarElemento.nodo){

        if(nodo.nombre == buscarElemento)
            this.busquedaElemento = nodo;

        if(nodo.has0wnProperty(v: "hIzq"))
            this.buscarNombre(buscarElemento,nodo.hIzq);

        if(nodo.has0wnProperty(v: "hDer"))
            this.buscarNombre(buscarElemento,nodo.hDer);

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo){
        if (nodo.padre == null){
            this.caminoNodo = this.caminoNodo + " " + nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.nombre +" "+ this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if (nodo.padre == null){
            this.sumaDeCaminos = this.sumaDeCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumaDeCaminos;
    }
}