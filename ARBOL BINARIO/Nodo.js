class Nodo{
    constructor(tipo,valor,nivel,padre,HijoI  = 0,HijoD  =0){
        this.tipo = tipo;
        this.valor = valor;
        this.nivel = nivel;
        this.HijoI = HijoI;
        this.HijoD = HijoD;
        if(HijoI==1)
            this.Hijoizquierdo = this.crearHijo("I",  0, nivel + 1, this.nivel);
        if(HijoD==1)
            this.Hijoderecho = this.crearHijo( "D",  0, nivel + 1, this.nivel);
    }
    crearHijo (tipo,valor,nivel,padre){
        var crearHijo = new Nodo(tipo,valor,nivel,padre);
        return crearHijo;
    }
}
