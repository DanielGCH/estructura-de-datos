class Nodo
{
    constructor(nombre,posicion, nivel, hijoI, hijoD,valor,padre=null)
    {
        this.nombre=nombre;
        this.posicion=posicion;
        this.nivel=nivel;
        this.hijoi=hijoI;
        this.hijod=hijoD
        this.valor=valor;
        this.padre=padre;
    }

    hijoI(nombre,posicion,nivel, hijoI, hijoD,valor,padre)
    {
        var Nod= new Nodo(nombre,posicion,nivel,hijoI,hijoD,valor,padre)
        return Nod
    }

    hijoD(nombre,posicion,nivel, hijoI, hijoD,valor,padre)
    {
        var Nod= new Nodo(nombre,posicion,nivel,hijoI,hijoD,valor,padre)
        return Nod
    }
}