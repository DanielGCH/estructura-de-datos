class Arbol
{
    constructor()
    {
        this.Padre=this.nodopadre;
        //this.nivel=3;
        //this.Bnivel=[];
        //this.Element;
        //this.Recorrido='';
        //this.Sumatotal=0;
    }

    agregarnodo(nombre,posicion,nivel,hijoI,hijoD,valor,padre)
    {
        var nod=new Nodo(nombre,posicion,nivel,hijoI,hijoD,valor,padre);
        return nod;
    }

    nodopadre(nombre,posicion,nivel,hijoI,hijoD,valor,padre)
    {
        var nod=new Nodo(nombre,posicion,nivel,hijoI,hijoD,valor,padre);
        return nod;
    }

    nivels(nodo)
    {
        if(nodo.nivel==this.nivel)
            this.Bnivel.push(nodo.valor);

        if(nodo.hasOwnProperty('hijoI'))
            this.nivels(nodo.hijoI);

        if(nodo.hasOwnProperty('hijoD'))
            this.nivels(nodo.hijoD);

        return this.Bnivel;
    }

    Bvalor(Element, nodo)
    {
        if(nodo.nombre==Element)
            this.Element=nodo;

        if(nodo.hasOwnProperty('hijoI'))
            this.Bvalor(Element,nodo.hijoI);

        if(nodo.hasOwnProperty('hijoD'))
            this.Bvalor(Element,nodo.hijoD);

        return this.Element;
    }

    Cnodo(nodo)
    {
        if(nodo.padre!=null)
        {

            this.Recorrido=this.Recorrido + ' ' +nodo.padre.nombre;

            this.Cnodo(nodo.padre)
        }
        return this.Element.nombre + ' ' + this.Recorrido;
    }
    Sderecorrido(nodo)
    {
        if(nodo.padre!=null)
        {

            this.Sumatotal=this.Sumatotal +nodo.padre.valor;

            this.Sderecorrido(nodo.padre)
        }
        return this.Element.valor + this.Sumatotal;

    }



}
